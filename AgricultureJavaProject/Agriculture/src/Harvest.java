/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author b2031120
 */

import java.util.Date;
import java.util.Calendar;

public class Harvest {
    int harvestId;
    Date timeHarvested;
    Date timeFrozen;
    float yield;
    Plot plotHarvested;
    
    Harvest(Plot enteredPlot, float enteredYield, Date enteredTmeHarvested)
    {
        plotHarvested = enteredPlot;
        yield = enteredYield;
        timeHarvested = enteredTmeHarvested;       
    }
    
    Date GetTimeharvested()
    {
        return timeHarvested;
    }
    
    Date GetTimeFrozen()
    {
        return timeFrozen;
    }
    
    float getYield()
    {
        return yield;
    }
    
    //todo: understand why return String of crop name rather than full plot?
//    String getCrop()
//    {
//    
//    }
    
    void setTimeFrozen()
    {
       // design suggests no parameters is time frozen using now?
       Calendar cal = Calendar.getInstance();
       timeFrozen = cal.getTime();
    }
}