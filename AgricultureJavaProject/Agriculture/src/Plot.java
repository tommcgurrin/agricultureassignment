/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author b2031120
 */
import java.util.Date;

public class Plot {
    int plotId;
    String name;
    int size;
    String soilType;
    SetOfCrops cropHistory;
    SetOfTreatments treatmentHistory;
    
    int GetPlotId()
    {
        return plotId;
    }
    
   String getName()
   {
       return name;
   }
   
   int getSize()
   {
       return size;
   }
   
   String getSoilType()
   {
       return soilType;
   }
   
   SetOfCrops getCropHistory()
   {
       return cropHistory;
   }
   
   SetOfTreatments getTreatmentHistory()
   {
       return treatmentHistory;
   }
   
   void setName(String plotName)
   {
       name = plotName; 
   }
   
   void setSize(int plotSize)
   {
       size = plotSize;
   }
   
   void setSoilType(String type)
   {
       soilType = type;
   }
   
   void emptyPlot()
   {
       //todo implement empty plot
   }
           
}
