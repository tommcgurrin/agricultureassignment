/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author b2031120
 */
import java.util.Vector;

public class SetOfCrops extends Vector<Crop>{
    Vector<Crop> cropSet;
    
    void addCrop(Crop cropToBeAdded)
    {
        cropSet.add(cropToBeAdded);
    }
    
    SetOfCrops findCropByName (String name)
    {
        SetOfCrops cropsWithName = new SetOfCrops();
        for(int i = 0; i < cropSet.size(); i++)
        {
            Crop crop = cropSet.get(i);
            if (crop.getName().contains(name))
            {
                cropsWithName.add(crop);
            }            
        }
        return cropsWithName;
    }
}
