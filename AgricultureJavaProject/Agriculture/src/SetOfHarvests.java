/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author b2031120
 */

import java.util.Vector;
import java.util.Date;

public class SetOfHarvests extends Vector<Harvest> {
    Vector<Harvest> harvestSet;
    
    void addHarvest(Harvest harvest)
    {
        harvestSet.add(harvest);
    }
    
    void createHarvest(Plot plot, float yield, Date timeHarvested)
    {
        //should this not return the dreated harvest?
        new Harvest(plot, yield, timeHarvested);       
    }

    public SetOfHarvests findHarvestBetweenDates(Date startTime, Date endTime) {
        
        SetOfHarvests harvestsBetweenDates = new SetOfHarvests();
        for(int i = 0; i < harvestSet.size(); i++)
        {
            Harvest currentHarvest = harvestSet.get(i);
            Date currentHarvestedDate = currentHarvest.GetTimeharvested();
            if ((currentHarvestedDate.before(endTime)) || currentHarvestedDate.after(startTime)
                    || currentHarvestedDate.equals(startTime) || currentHarvestedDate.equals(endTime))
            {
                harvestsBetweenDates.add(currentHarvest);
            }
        }
        return harvestsBetweenDates;
    }
    
    public SetOfHarvests findHarvestBetweenFrozenTimes(Date startTime, Date endTime) {
        
        SetOfHarvests harvestsBetweenFrozenDates = new SetOfHarvests();
        for(int i = 0; i < harvestSet.size(); i++)
        {
            Harvest currentHarvest = harvestSet.get(i);
            Date currentHarvestFrozenDate = currentHarvest.GetTimeFrozen();
            if ((currentHarvestFrozenDate.before(endTime)) || currentHarvestFrozenDate.after(startTime)
                    || currentHarvestFrozenDate.equals(startTime) || currentHarvestFrozenDate.equals(endTime))
            {
                harvestsBetweenFrozenDates.add(currentHarvest);
            }
        }
        return harvestsBetweenFrozenDates;
    }
    
    // this was implemented with one folat paramteter if it between two we need two limits, so two parameters
    public SetOfHarvests findHarvestBetweenYield(float lowerYield, float higherYield)
    {
        SetOfHarvests harvestWithinYieldRange = new SetOfHarvests();
        for(int i = 0; i < harvestSet.size(); i++)
        {
            Harvest currentHarvest = harvestSet.get(i);
            float currentYield = currentHarvest.getYield();
            if (currentYield >= lowerYield || currentYield <= higherYield)
            {
                harvestWithinYieldRange.add(currentHarvest);
            }
        }
        return harvestWithinYieldRange;
    }
    
    //seeing as though it had one parameter in design, maybe you want to find by an exact yield so:
    
    public SetOfHarvests findHarvestByYield(float yield)
    {
        SetOfHarvests harvestsByYield = new SetOfHarvests();
        for(int i = 0; i < harvestSet.size(); i++)
        {
            Harvest currentHarvest = harvestSet.get(i);
            if(currentHarvest.getYield() == yield)
            {
                harvestsByYield.add(currentHarvest);
            }
        }
        return harvestsByYield;
    }      
}
