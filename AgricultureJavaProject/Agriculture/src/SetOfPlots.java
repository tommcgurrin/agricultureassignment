/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Vector;

/**
 *
 * @author b2031120
 */

public class SetOfPlots extends Vector<Plot> {
    Vector<Plot> plotSet;
    
    void addPlot(Plot plot)
    {
        plotSet.add(plot);
    }
    
    void deletePlot(Plot plot)
    {
        plotSet.remove(plot);
    }
    
    void harvestPlot(Plot plot)
    {
        //todo;
    }
    
    Plot findPlotById (int plotId)
    {
        for (int i = 0; i < plotSet.size(); i++)
        {
           Plot presentPlot = plotSet.get(i);
           if (presentPlot.GetPlotId() == plotId)
           {
               return presentPlot;
           }                    
        }
        
        return null;
    }

    SetOfPlots findPlotsBetweenSize(int minSize, int maxSize) 
    {
        SetOfPlots plotsInSizeRange = new SetOfPlots();
        for (int i = 0; i < plotSet.size(); i++)
        {
           Plot presentPlot = plotSet.get(i);
           if ((presentPlot.size > minSize) || (presentPlot.size < maxSize))
           {
               plotsInSizeRange.add(presentPlot);
           }                    
        }
        return plotsInSizeRange;
    }
    
    SetOfPlots findPlotBySoilType(String soilType)
    {
        SetOfPlots plotsWithSoilType = new SetOfPlots();
        for (int i = 0; i < plotSet.size(); i++)
        {
           Plot presentPlot = plotSet.get(i);
           if (presentPlot.soilType == soilType)
           {
               plotsWithSoilType.add(presentPlot);
           }                    
        }
        return plotsWithSoilType;
    }
    
    SetOfPlots FindPlotsByCrop(String CropName)   
    {
        SetOfPlots plotsWithSoilType = new SetOfPlots();
        for (int i = 0; i < plotSet.size(); i++)
        {
           Plot presentPlot = plotSet.get(i);
           if (presentPlot.getCropHistory().findCropByName(CropName) != null)
           {
               plotsWithSoilType.add(presentPlot);
           }                    
        }
        return plotsWithSoilType;
    }
    
//todo: consider treatmentImplementation
//    SetOfPlots findPlotByFertiliserBrand(String fertiliserBrand)
//    {
//        String fertiliserTreatmentType = "fertiliser";
//        SetOfPlots plotsWithSoilType = new SetOfPlots();
//        for (int i = 0; i < plotSet.size(); i++)
//        {
//           Plot presentPlot = plotSet.get(i);
//           SetOfTreatments fertilisedTreatments = presentPlot.getTreatmentHistory().findTreatmentByType(fertiliserTreatmentType);
//           for(int j = 0; j < fertilisedTreatments.size(); j++)
//           {
//               fertilisedTreatments(j).
//           }
//           {
//              
//               plotsWithSoilType.add(presentPlot);
//           }                    
//        }
//        return plotsWithSoilType;
//    }
    
    
}
