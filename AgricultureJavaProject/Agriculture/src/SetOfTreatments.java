/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Vector;
import java.util.Date;

/**
 *
 * @author b2031120
 */
public class SetOfTreatments extends Vector<TreatmentApplication> {
 
    Vector<TreatmentApplication> listTreatments;
            
    void addTreatment(TreatmentApplication treatment)
    {
        listTreatments.add(treatment);
    }
    
    SetOfTreatments findTreatmentByType (String type)
    {
        SetOfTreatments treatmentsOfSpecifiedType = new SetOfTreatments();
                
        for(int i = 0; i < listTreatments.size(); i++)
        {
            TreatmentApplication treatment = listTreatments.get(i);
            if (treatment.GetType() == type)
            {
                treatmentsOfSpecifiedType.add(treatment);
            }
        }
        
        return treatmentsOfSpecifiedType;
    }
    
    SetOfTreatments findTreatmentBetweenDate(Date startTime, Date endTime)
    {
        SetOfTreatments treatmentsWithinDateRange = new SetOfTreatments();
                
        for(int i = 0; i < listTreatments.size(); i++)
        {
            TreatmentApplication treatment = listTreatments.get(i);
            Date dateDone = treatment.GetDateDone();
            if (dateDone == startTime || dateDone == endTime || 
                    (dateDone.after(startTime) && dateDone.after(endTime) ))
            {
                treatmentsWithinDateRange.add(treatment);
            }
        }
        
        return treatmentsWithinDateRange;
    }

//    Todo:
//    TreatmentApplication findLatestTreatment()
//    {
//
//    }
    
    
}
